
import conf  from "../config.js";
import gulp  from "gulp";
import watch from "gulp-watch";

// Watch
gulp.task("watch", () => {
  conf.watch.tasks.forEach((t) => {
    watch(t.path, () => {
      gulp.start(t.task);
    });
  });
});
