
import babelify   from "babelify";
import browserify from "browserify";
import buf        from "gulp-buffer";
import conf       from "../config.js";
import gulp       from "gulp";
import header     from "gulp-header";
import notify     from "gulp-notify";
import pkg        from "../../package.json";
import plumber    from "gulp-plumber";
import rename     from "gulp-rename";
import tap        from "gulp-tap";
import uglify     from "gulp-uglify";
import utility    from "../utility.js";

// JS
gulp.task("js", ["jslint"], () => {
  return gulp.src(conf.js.input, { read: false })
    .pipe(plumber({ errorHandler: utility.errorHandler }))
    .pipe(tap((file) => {
      file.contents = browserify(file.path, {
        debug: true,
        extensions: [".js"]
      })
      .transform("envify", {
        PROJECT_ENV: process.env.PROJECT_ENV
      })
      .transform(babelify.configure({
        presets: ["es2015"],
        extensions: [".js"]
      }))
      .bundle()
      .on("error", function (err) {
        utility.errorHandler(err, this);
        this.emit("end");
      });
    }))
    .pipe(buf())
    .pipe(uglify())
    .pipe(header(conf.banner, { pkg: pkg, timestamp: Math.floor(Date.now() / 1000) }))
    .pipe(rename("app.js"))
    .pipe(gulp.dest(conf.js.output))
    .pipe(notify({ title: "JS", message: "✓ JS task complete", onLast: true }));
});
