
import conf from "../config.js";
import gulp from "gulp";
import ps   from "project-server";

// Styleguide
gulp.task("styleguide", () => {
  return ps.projectServerStyleguide(conf.server.input, { config: conf.server.projectServer });
});
