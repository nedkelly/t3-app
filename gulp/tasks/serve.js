
import bs   from "browser-sync";
import conf from "../config.js";
import gulp from "gulp";
import ps   from "project-server";

// Serve
gulp.task("serve", () => {
  bs.init({
    files: [conf.server.output],
    reloadDebounce: 1000,
    server: {
      baseDir: conf.server.input,
      directory: true,
      middleware: [ps.projectServerIndex(conf.server.input, { config: conf.server.projectServer })],
    },
    notify: false,
    online: false,
    open: "local"
  });
});
