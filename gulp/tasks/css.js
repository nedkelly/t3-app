
import bourbon from "node-bourbon";
import combine from "gulp-combine-mq";
import conf    from "../config.js";
import cssmin  from "gulp-cssmin";
import gulp    from "gulp";
import header  from "gulp-header";
import notify  from "gulp-notify";
import pkg     from "../../package.json";
import plumber from "gulp-plumber";
import sass    from "gulp-sass";
import utility from "../utility.js";

// CSS
gulp.task("css", () => {
  return gulp.src(conf.css.input)
    .pipe(plumber({ errorHandler: utility.errorHandler }))
    .pipe(sass({ outputStyle: "expanded", includePaths: bourbon.includePaths }).on("error", sass.logError))
    .pipe(combine({ beautify: false }))
    .pipe(cssmin())
    .pipe(header(conf.banner, { pkg: pkg, timestamp: Math.floor(Date.now() / 1000) }))
    .pipe(gulp.dest(conf.css.output))
    .pipe(notify({ title: "CSS", message: "✓ CSS task complete", onLast: true }));
});
