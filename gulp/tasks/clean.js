
import conf from "../config.js";
import del  from "del";
import gulp from "gulp";

// Clean
gulp.task("clean", (cb) => {
  return del([conf.clean.input], cb);
});
