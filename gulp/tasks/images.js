
import cache    from "gulp-cache";
import conf     from "../config.js";
import gulp     from "gulp";
import imagemin from "gulp-imagemin";
import notify   from "gulp-notify";
import plumber  from "gulp-plumber";
import utility  from "../utility.js";

// Images
gulp.task("images", () => {
  return gulp.src(conf.images.input)
    .pipe(plumber({ errorHandler: utility.errorHandler }))
    .pipe(cache(imagemin({ optimizationLevel: 3, progressive: true, interlaced: true })))
    .pipe(gulp.dest(conf.images.output))
    .pipe(notify({ title: "Images", message: "✓ Images task complete", onLast: true }));
});
