
import conf    from "../config.js";
import gulp    from "gulp";
import notify  from "gulp-notify";
import plumber from "gulp-plumber";
import utility from "../utility.js";

// HTML
gulp.task("html", () => {
  return gulp.src(conf.html.input)
    .pipe(plumber({ errorHandler: utility.errorHandler }))
    .pipe(gulp.dest(conf.html.output))
    .pipe(notify({ title: "HTML", message: "✓ HTML task complete", onLast: true }));
});
