
import gulp from "gulp";

// Prod Build
gulp.task("prod", () => {
   process.env.PROJECT_ENV = "prod";
   gulp.start(["css", "js", "images", "html"]);
});
