
import conf    from "../config.js";
import eslint  from "gulp-eslint";
import gulp    from "gulp";
import plumber from "gulp-plumber";
import utility from "../utility.js";

// JS Lint
gulp.task("jslint", () => {
    return gulp.src(conf.js.app)
    .pipe(plumber({ errorHandler: utility.errorHandler }))
    .pipe(eslint({
      fix: true
    }))
    .pipe(eslint.format("codeframe"));
});
