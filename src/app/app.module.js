const Box = require("t3js/dist/t3");
require("./app.service");

module.exports = Box.Application.addModule("app.module", (context) => {

  let _moduleEl,
      _service;

  const _render = () => {
    _service.empty(_moduleEl);
    _service.template(_moduleEl);
  };

  const init = () => {
    _moduleEl = context.getElement();
    _service  = context.getService("app.service");
    _render();
  };

  return {
    init: init
  };

});
