const Box = require("t3js/dist/t3");

module.exports = Box.Application.addService("app.service", (application) => {

  const template = (el) => {
    let node = document.createElement("H1");
    let text = document.createTextNode("t3 Start App - Hello World!");
    node.appendChild(text);
    el.appendChild(node);
  };

  const empty = (el) => {
    while (el.firstChild) {
      el.removeChild(el.firstChild);
    }
  };

  return {
    template: template,
    empty:    empty,
  };

});
