const Box = require("t3js/dist/t3");

require("./app/app.module");

module.exports = Box.Application.init({
  debug: (process.env.PROJECT_ENV !== "prod") // Node Env
});
